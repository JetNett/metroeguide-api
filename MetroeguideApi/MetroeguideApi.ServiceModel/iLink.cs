﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;

namespace MetroeguideApi.ServiceModel
{
    [DataContract]
    public class iLinkCommunityProfileRequest : IReturn<iLinkCommunityProfileResponse>
    {
        [DataMember(Order=1)]
        public int ClientId { get; set; }
        [DataMember(Order = 4)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 5)]
        public bool OrderByDesc { get; set; }
    }

    [DataContract]
    public class iLinkCommunityProfileResponse : IHasTimings
    {
        [DataMember(Order = 1)]
        public List<PageDto> Result { get; set; }

        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }

        [DataMember(Order =3)]
        public int ResultCount { get; set; }
    }
    [DataContract]
    public class iLinkRequest : IReturn<iLinkResponse>
    {
        [DataMember(Order = 1)]
        public int ClientId { get; set; }

        [DataMember(Order = 2)]
        public int? Limit { get; set; }

        [DataMember(Order = 3)]
        public int Skip { get; set; }

        [DataMember(Order = 4)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 5)]
        public bool OrderByDesc { get; set; }
    }

    [DataContract]
    public class iLinkResponse : IHasTimings
    {
        [DataMember(Order = 1)]
        public List<iLinkDto> Result { get; set; }

        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }

        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
        [DataMember(Order = 4)]
        public int? Skip { get; set; }

        [DataMember(Order = 5)]
        public int? Limit { get; set; }
    }
}
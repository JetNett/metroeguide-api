﻿using MetroeguideApi.ServiceModel.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel
{
    public class UrlResponse
    {
        public List<UrlDto> Result { get; set; }
    }

    public class UrlRequest
    {
        public int Id { get; set; }
    }
}

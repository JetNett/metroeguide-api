﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel
{
    public class CheckedLinkRequest
    {
        public string Id { get; set; }

        public int? Limit { get; set; }

        public int Skip { get; set; }

        public string Type { get; set; }
    }

    public class CheckedLinkUrlRequest
    {
        public string Id { get; set; }
        public int UrlId { get; set; }
        public string Url { get; set; }
    }

    public class DeleteCheckedLinkRequest
    {
        public string Id { get; set; }
    }

    public class DeactivateLinkRequest
    {
        public int UrlId { get; set; }
        public string Url { get; set; }
    }

    public class SuspendCheckedLinkRequest
    {
        public int UrlId { get; set; }
        public string Url { get; set; }
    }

    public class CommentCheckedLinkRequest
    {
        public int UrlId { get; set; }
        public string Url { get; set; }
        public string Comment { get; set; }
    }

}

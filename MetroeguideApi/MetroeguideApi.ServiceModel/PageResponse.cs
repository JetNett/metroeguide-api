﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;

namespace MetroeguideApi.ServiceModel
{
    [DataContract]
    public class DeletePageRequest
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
    }


    [DataContract]
    public class PageResponse : IHasTimings
    {
        [DataMember(Order = 1)]
        public List<PageDto> Result { get; set; }
        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }
        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
        [DataMember(Order = 4)]
        public int? Skip { get; set; }
        [DataMember(Order = 5)]
        public int? Limit { get; set; }
    }

    public class PageRequest : IReturn<PageResponse>
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int FolderId { get; set; }

        [DataMember(Order = 3)]
        public string Title { get; set; }

        [DataMember(Order = 4)]
        public string Route { get; set; }

        [DataMember(Order = 5)]
        public string CanonicalUrl { get; set; }

        [DataMember(Order = 6)]
        public int? Limit { get; set; }

        [DataMember(Order = 7)]
        public int? Skip { get; set; }

        [DataMember(Order = 8)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 9)]
        public bool OrderByDesc { get; set; }

        [DataMember(Order = 10)]
        public string Zipcode { get; set; }
    }
}
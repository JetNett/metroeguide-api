﻿using MetroeguideApi.ServiceModel.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel
{
    public class CheckedLinkResponse
    { 
        public List<CheckedLinkDto> Result { get; set; }

        public int ResultCount { get; set; }

        public int? Skip { get; set; }

        public int? Limit { get; set; }
    }
}

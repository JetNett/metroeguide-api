﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel
{
    public class WeatherResponse
    {
        public string CurrentCondition { get; set; }
        public int CurrentTemp { get; set; }
        public int MaxTemp { get; set; }
        public int MinTemp { get; set; }
        public int IconId { get; set; }
    }
}

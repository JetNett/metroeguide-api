﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;

namespace MetroeguideApi.ServiceModel
{

    [DataContract]
    public class DeleteLinkRequest
    {
        [DataMember(Order=1)]
        public int Id { get; set; }
    }

[DataContract]
    public class LinkRequest : IReturn<LinkResponse>
    {
        [DataMember(Order=1)]
        public int Id { get; set; }
        [DataMember(Order=2)]
        public int UrlId { get; set; }
        [DataMember(Order=3)]
        public int PageId { get; set; }
        [DataMember(Order=4)]
        public string Title { get; set; }
        [DataMember(Order=5)]
        public string Url { get; set; }
        [DataMember(Order=6)]
        public int? Limit { get; set; }
        [DataMember(Order=7)]
        public bool? IsSuspended { get; set; }
        [DataMember(Order=8)]
        public bool? IsElevated { get; set; }
        [DataMember(Order=9)]
        public int Skip { get; set; }
        [DataMember(Order=10)]
        public string[] OrderBy { get; set; }
        [DataMember(Order=11)]
        public bool OrderByDesc { get; set; }
    }

    public class BulkLinkRequest
    {
        public List<LinkDto> Links { get; set; }
    }

    public class BulkLinkDeleteRequest
    {
        public int PageId { get; set; }
    }

    [DataContract]
    public class LinkResponse : IHasTimings
    {
        [DataMember(Order=1)]
        public List<LinkDto> Result { get; set; }
        [DataMember(Order=2)]
        public long TimeTakenMs { get; set; }
        [DataMember(Order=3)]
        public int ResultCount { get; set; }
        [DataMember(Order=4)]
        public int? Skip { get; set; }
        [DataMember(Order=5)]
        public int? Limit { get; set; }
    }

    public class FindLinksRequest
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public bool UseSimilar { get; set; }
    }
}
﻿namespace MetroeguideApi.ServiceModel.Types
{
    public interface IHasTimings
    {
        long TimeTakenMs { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel.Types
{
    public class WhitelistedUrlDto
    {
        public Guid Id { get; set; }
        public int UrlId { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateExpires { get; set; }
    }

    public class CreateWhitelistedUrlRequest
    {
        public int UrlId { get; set; }
    }

    public class RemoveWhitelistedUrlRequest
    {
        public string Id { get; set; }
    }
}

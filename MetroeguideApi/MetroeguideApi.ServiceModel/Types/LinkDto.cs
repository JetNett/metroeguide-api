﻿using ServiceStack.DataAnnotations;
using System;
using System.Runtime.Serialization;

namespace MetroeguideApi.ServiceModel.Types
{
    public class LinkDto
    {
        [AutoIncrement]
        public int Id { get; set; }

        [DataMember(Order=2)]
        public int UrlId { get; set; }

        [DataMember(Order=3)]
        public string Title { get; set; }

        [DataMember(Order=4)]
        public string Url { get; set; }

        [DataMember(Order=5)]
        public bool IsLink { get; set; }

        [DataMember(Order=6)]
        public int PageId { get; set; }

        [DataMember(Order=7)]
        public int? Position { get; set; }

        [DataMember(Order=8)]
        public string Target { get; set; }

        [DataMember(Order=9)]
        public string Comments { get; set; }

        [DataMember(Order=10)]
        public bool? IsSuspended { get; set; }

        [DataMember(Order=11)]
        public bool? IsElevated { get; set; }
        
        [DataMember(Order=12)]
        public DateTime Modified { get; set; }
    }
}
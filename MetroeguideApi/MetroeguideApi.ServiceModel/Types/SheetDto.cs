﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;

namespace MetroeguideApi.ServiceModel.Types
{
    public class SheetDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? PublishedAt { get; set; }
    }

    public class SheetLinkDto
    {
        [AutoIncrement]
        public int Id { get; set; }
        public int PageId { get; set; }
        public int SheetId { get; set; }
        public int Position { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Target { get; set; }
        public bool IsLink { get; set; }
    }

    public class SheetRequest
    {
        public int Id { get; set; }
    }

    public class SheetResponse
    {
        public List<SheetDto> Result { get; set; }
    }

    public class BulkSheetLinkRequest
    {
        public List<SheetLinkDto> SheetLinks { get; set; }
    }

    public class SheetLinkRequest
    {
        public int SheetId { get; set; }
    }

    public class SheetLinkResponse
    {
        public List<SheetLinkDto> Result { get; set; }
    }

    public class PublishSheetRequest
    {
        public int Id { get; set; }
    }
}

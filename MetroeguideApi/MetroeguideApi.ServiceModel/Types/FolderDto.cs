﻿using System;
using System.Runtime.Serialization;

namespace MetroeguideApi.ServiceModel.Types
{
    [DataContract]
    public class FolderDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? ParentFolderId { get; set; }
        
        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public DateTime Modified { get; set; }
    }

    public class DeleteFolderRequest
    {
        public int Id { get; set; }
        public bool HasChildren { get; set; }
    }
}
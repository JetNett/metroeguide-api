﻿using System;
using System.Runtime.Serialization;

namespace MetroeguideApi.ServiceModel.Types
{
    [DataContract]
    public class PageDto
    {
        [DataMember(Order=1)]
        public int Id { get; set; }

        [DataMember(Order=2)]
        public int FolderId { get; set; }

        [DataMember(Order=3)]
        public string Title { get; set; }

        [DataMember(Order=4)]
        public bool AutoOrdering { get; set; }

        [DataMember(Order=5)]
        public string FooterHtml { get; set; }

        [DataMember(Order=6)]
        public string HeaderHtml { get; set; }

        [DataMember(Order=7)]
        public string MetaKeys { get; set; }

        [DataMember(Order=8)]
        public string MetaDesc { get; set; }

        [DataMember(Order=9)]
        public string Route { get; set; }

        [DataMember(Order=10)]
        public string CanonicalUrl { get; set; }

        [DataMember(Order=11)]
        public string Comments { get; set; }

        [DataMember(Order=12)]
        public string Zipcode { get; set; }

        [DataMember(Order=13)]
        public DateTime Modified { get; set; }

    }

    [DataContract]
    public class BulkPageUpdate
    {
        [DataMember(Order = 1)]
        public int NewParentFolderId { get; set; }
        [DataMember(Order = 2)]
        public string PageIds { get; set; }
    }

    public class FindPageRequest
    {
        public int FindById { get; set; }
        public string FindByName { get; set; }
    }
}
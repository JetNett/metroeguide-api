﻿using System;
using System.Runtime.Serialization;

namespace MetroeguideApi.ServiceModel.Types
{
    [DataContract]
    public class ClientDto
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }
         
        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public string UserId { get; set; }

        [DataMember(Order = 4)]
        public string Password { get; set; }

        [DataMember(Order = 5)]
        public string Website { get; set; }

        [DataMember(Order = 6)]
        public string DisplayName { get; set; }

        [DataMember(Order = 7)]
        public string Email { get; set; }

        [DataMember(Order = 8)]
        public string Css { get; set; }

        [DataMember(Order = 9)]
        public string AnalyticsKey { get; set; }

        [DataMember(Order = 10)]
        public string Comments { get; set; }

        [DataMember(Order = 11)]
        public DateTime Modified { get; set; }
    }
}
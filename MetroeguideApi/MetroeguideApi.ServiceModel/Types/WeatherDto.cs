﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel.Types
{

    public class WeatherDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public WeatherDetails[] Weather { get; set; }
        [JsonProperty("main")]
        public TempDetails TempDetails { get; set; }
    }

    public class WeatherDetails
    {
        public int Id { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
    }

    public class TempDetails
    {
        public decimal Temp { get; set; }
        public decimal Pressure { get; set; }
        public decimal Humidity { get; set; }
        [JsonProperty("temp_min")]
        public decimal TempMin { get; set; }
        [JsonProperty("temp_max")]
        public decimal TempMax { get; set; }
    }

    public class WeatherRequest
    {
        public int Zipcode { get; set; }
    }
}

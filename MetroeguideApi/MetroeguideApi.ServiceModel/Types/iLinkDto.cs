﻿using System;
using System.Runtime.Serialization;

namespace MetroeguideApi.ServiceModel.Types
{
    [DataContract]
    public class iLinkDto
    {
        [DataMember (Order=1)]
        public int Id { get; set; } 

        [DataMember (Order=2)]
        public string PageBgColor { get; set; }

        [DataMember (Order=3)]
        public string PageLinkColor { get; set; }

        [DataMember (Order=4)]
        public string HomeSearchAnchorText { get; set; }

        [DataMember (Order=5)]
        public string HomeSearchAnchorHref { get; set; }

        [DataMember (Order=6)]
        public string HomeSearchHeaderText { get; set; }

        [DataMember (Order=7)]
        public string OriginationPage { get; set; }

        [DataMember (Order=8)]
        public string BackAnchorText { get; set; }

        [DataMember (Order=9)]
        public string BackAnchorUrl { get; set; }

        [DataMember (Order=10)]
        public string BackAnchorTarget { get; set; }

        [DataMember (Order=11)]
        public string CommunityProfileIds { get; set; }

        [DataMember (Order=12)]
        public string ClientLogoImageSrc { get; set; }

        [DataMember (Order=13)]
        public string ClientLogoImageHref { get; set; }

        [DataMember (Order=14)]
        public string ClientLogoImageTarget { get; set; }

        [DataMember (Order=15)]
        public string ClientLogoImageAlt { get; set; }

        [DataMember (Order=16)]
        public string ProductLogoImageSrc { get; set; }

        [DataMember (Order=17)]
        public string ProductLogoImageHref { get; set; }

        [DataMember (Order=18)]
        public string ProductLogoImageTarget { get; set; }

        [DataMember (Order=19)]
        public string ProductLogoImageAlt { get; set; }

        [DataMember (Order=20)]
        public int? FontSizePx { get; set; }

        [DataMember (Order=21)]
        public string SeoMetaKeys { get; set; }

        [DataMember (Order=22)]
        public string SeoMetaDesc { get; set; }

        [DataMember (Order=23)]
        public int ClientId { get; set; }

        [DataMember (Order=24)]
        public DateTime? Modified { get; set; }
    }

    public class DeleteILinkRequest
    {
        public int Id { get; set; }
    }
}
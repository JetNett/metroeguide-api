﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel.Types
{
    public class UrlDto
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public bool Suspended { get; set; }
        public string Comments { get; set; }
    }

    public class UpdateUrlRequest
    {
        public int Id { get; set; }
        public string Url { get; set; }
    }

    public class SuspendUrlRequest
    {
        public int Id { get; set; }
        public bool Suspended { get; set; }
    }

    public class RemoveUrlRequest
    {
        public int Id { get; set; }
    }

    public class UrlWhitelistRequest
    {
        public int Id { get; set; }
        public bool Whitelisted { get; set; }
    }

    public class CommentUrlRequest
    {
        public int Id { get; set; }
        public string Comment { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceModel.Types
{
    public class CheckedLinkDto
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string EndUrl { get; set; }
        public int UrlId { get; set; }
        public int Status { get; set; }
        public string ExceptionMessage { get; set; }
        public string Comments { get; set; }
        public DateTime TimeChecked { get; set; }

    }
}

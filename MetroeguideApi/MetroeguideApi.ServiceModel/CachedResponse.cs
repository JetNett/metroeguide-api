﻿using System;
using System.Runtime.Serialization;

namespace MetroeguideApi.ServiceModel
{
    [DataContract]
    public class CachedResponseWrapper
    {
        [DataMember(Order=1)]
        public byte[] ProtoBufResponse { get; set; } 
        [DataMember(Order=2)]
        public DateTime CacheTime { get; set; }
    }
}
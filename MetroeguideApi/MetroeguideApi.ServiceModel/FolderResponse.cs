﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MetroeguideApi.ServiceModel.Types;

namespace MetroeguideApi.ServiceModel
{
    [DataContract]
    public class FolderResponse : IHasTimings
    {
        [DataMember]
        public List<FolderDto> Result { get; set; }

        [DataMember]
        public long TimeTakenMs { get; set; }

        [DataMember]
        public int ResultCount { get; set; }

        [DataMember]
        public int? Skip { get; set; }

        [DataMember]
        public int? Limit { get; set; }

    }

    [DataContract]
    public class FolderRequest
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public int? ParentFolderId { get; set; }

        [DataMember(Order = 4)]
        public int? Limit { get; set; }

        [DataMember(Order = 4)]
        public int Skip { get; set; }

        [DataMember(Order = 5)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 6)]
        public bool OrderByDesc { get; set; }
    }

    public class FolderHierarchyRequest
    {
        public int Id { get; set; }
        public bool GetHierarchy { get; set; }
    }

    public class FolderHierarchyResponse
    {
        [DataMember]
        public List<FolderDto> Result { get; set; }
    }

    public class FindFolderRequest
    {
        public int FindById { get; set; }
        public string FindByName { get; set; }
    }

}
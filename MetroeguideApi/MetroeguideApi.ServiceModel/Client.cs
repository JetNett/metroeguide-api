﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;

namespace MetroeguideApi.ServiceModel
{
    [DataContract]
    public class ClientRequest : IReturn<ClientResponse>
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public int? Limit { get; set; }

        [DataMember(Order = 3)]
        public int Skip { get; set; }

        [DataMember(Order = 4)]
        public string[] OrderBy { get; set; }

        [DataMember(Order = 5)]
        public bool OrderByDesc { get; set; }

        [DataMember(Order = 6)]
        public bool Condensed { get; set; }

    }

    [DataContract]
    public class ClientResponse : IHasTimings
    {
        [DataMember(Order = 1)]
        public List<ClientDto> Result { get; set; }

        [DataMember(Order = 2)]
        public long TimeTakenMs { get; set; }

        [DataMember(Order = 3)]
        public int ResultCount { get; set; }
        [DataMember(Order = 4)]
        public int? Skip { get; set; }

        [DataMember(Order = 5)]
        public int? Limit { get; set; }
    }

    public class DeleteClientRequest
    {
        public int Id { get; set; }
    }
}
﻿using Funq;
using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using MetroeguideApi.ServiceInterface;
using ServiceStack.Api.Swagger;
using ServiceStack.Caching;
using ServiceStack.Configuration;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.PostgreSQL;
using ServiceStack.Redis;
using ServiceStack.Text;
using StackExchange.Redis;
using System.Collections.Generic;

namespace MetroeguideApi
{
    public class AppHost : AppHostBase
    {
        /// <summary>
        /// Default constructor.
        /// Base constructor requires a name and assembly to locate web service classes. 
        /// </summary>
        public AppHost()
            : base("Metro eGuide Api", typeof(LinkService).Assembly)
        {

        }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        /// <param name="container"></param>
        public override void Configure(Container container)
        {
            JsConfig.DateHandler = DateHandler.ISO8601;
            JsConfig.EmitCamelCaseNames = true;

            //Config examples
            this.Plugins.Add(new PostmanFeature());
            this.Plugins.Add(new CorsFeature());
            this.Plugins.Add(new SwaggerFeature());

            container.Register<IDbConnectionFactory>(c => new OrmLiteConnectionFactory(ConfigUtils.GetConnectionString("MetroeguidePostgres"),
                PostgreSqlDialect.Provider));
            container.Register(c => ConnectionMultiplexer.Connect("redis.metroeguide.com:6379,Password=Mad15onmetr0"));
            container.Register<IDatabase>(c => c.Resolve<ConnectionMultiplexer>().GetDatabase());


            OrmLiteConfig.DialectProvider = PostgreSqlDialect.Provider;
            OrmLiteConfig.DialectProvider.NamingStrategy = new AliasNamingStrategy
            {
                TableAliases =
                {
                    {"LinkDto", "links"},
                    {"PageDto", "pages"},
                    {"iLinkDto", "i_links"},
                    {"ClientDto", "clients"},
                    {"FolderDto", "folders"},
                    {"SheetDto", "mp_sheets" },
                    {"SheetLinkDto", "mp_sheet_links" },
                    {"CheckedLinkDto", "link_checker_logs_new" },
                    {"UrlDto", "urls"},
                    {"WhitelistedUrlDto", "link_checker_whitelist_new" }
                },
                UseNamingStrategy = new PostgreSqlNamingStrategy()
            };
           
            RouteConfiguration();
        }

        public void RouteConfiguration()
        {
            //links
            Routes
                .Add<LinkRequest>("/links", "GET");

            Routes
                .Add<DeleteLinkRequest>("/link", "DELETE");

            Routes
                .Add<LinkDto>("/links", "POST, PUT")
                .Add<FindLinksRequest>("/links/find", "GET")
                .Add<BulkLinkDeleteRequest>("/links/bulkByPage", "DELETE");

            Routes
                .Add<BulkLinkRequest>("/links/bulk", "POST, PUT, DELETE");

            Routes
                .Add<PageRequest>("/pages", "GET")
                .Add<PageDto>("/pages", "PUT, POST")
                .Add<FindPageRequest>("/pages/find", "GET");

           Routes
                .Add<BulkPageUpdate>("/pages/bulkUpdate", "PUT");

            Routes
                .Add<DeletePageRequest>("/page", "DELETE");

            Routes
                .Add<iLinkRequest>("/ilinks", "GET")
                .Add<iLinkDto>("/ilinks", "POST, PUT")
                .Add<DeleteILinkRequest>("/ilinks", "DELETE")
                .Add<iLinkCommunityProfileRequest>("/ilinks/{ClientId}/communityProfiles", "GET");

            Routes
                .Add<ClientRequest>("/clients", "GET")
                .Add<ClientDto>("/clients", "POST, PUT")
                .Add<DeleteClientRequest>("/clients", "DELETE");

            Routes
                .Add<Heartbeat>("/heartbeat", "GET");


            Routes
                .Add<FolderRequest>("/folders", "GET")
                .Add<DeleteFolderRequest>("/folders", "DELETE")
                .Add<FolderDto>("/folders", "POST")
                .Add<FolderHierarchyRequest>("/folderHierarchy", "GET")
                .Add<FindFolderRequest>("/folders/find", "GET");

            Routes
                .Add<SheetRequest>("/sheets", "GET")
                .Add<SheetLinkRequest>("/sheets/links", "GET, DELETE")
                .Add<SheetDto>("/sheets", "POST")
                .Add<BulkSheetLinkRequest>("/sheets/links", "POST")
                .Add<PublishSheetRequest>("/sheets/publish", "PUT");

            Routes.Add<CommunityProfileDto>("/communityProfiles", "POST, DELETE");

            Routes.Add<CheckedLinkRequest>("/linkChecker", "GET");
            Routes.Add<CheckedLinkUrlRequest>("/linkChecker", "PUT");
            Routes.Add<DeleteCheckedLinkRequest>("/linkChecker", "DELETE");

            Routes.Add<CreateWhitelistedUrlRequest>("/linkChecker/whitelist", "POST");
            Routes.Add<SuspendCheckedLinkRequest>("/linkChecker/suspend", "PUT");
            Routes.Add<DeactivateLinkRequest>("/linkChecker/deactivate", "DELETE");
            Routes.Add<CommentCheckedLinkRequest>("/linkChecker/comment", "PUT");

            Routes.Add<UrlRequest>("/urls", "GET");
            Routes.Add<UpdateUrlRequest>("/urls", "PUT");
                    
            Routes.Add<UrlWhitelistRequest>("/ulrs/whitelist", "POST");

            Routes.Add<WeatherRequest>("/weather/{zipcode}", "GET");
        }
    }
}
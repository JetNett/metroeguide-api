﻿using Chronos.Dapper.Chronos.Dapper;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceInterface
{
    public class SheetService : Service
    {
        public SheetResponse Get(SheetRequest request)
        {

            var result = default(List<SheetDto>);

            var expression = Db.From<SheetDto>();

            if (request.Id != default(int))
            {
                result = new[] { Db.Single<SheetDto>(x => x.Id == request.Id) }
                    .ToList();
            }
            else
            {
                result = Db.Select<SheetDto>().Where(x => x.PublishedAt == null).ToList();
            }

            var response = new SheetResponse();
            response.Result = result;
            return response; 
        }

        public SheetLinkResponse Get(SheetLinkRequest request)
        {
            var result = default(List<SheetLinkDto>);
            result = Db.Select<SheetLinkDto>().Where(x => x.SheetId == request.SheetId).ToList();

            var response = new SheetLinkResponse();
            response.Result = result;
            return response;
        }

        public HttpResult Post(SheetDto sheet)
        {
            sheet.CreatedAt = DateTime.Now;
            var iden = Db.ExecuteScalar<int>("insert into mp_sheets (name, created_at) values (@name, @createdAt) returning id;", sheet);
            return new HttpResult(iden, HttpStatusCode.Created);
        }

        public HttpResult Post(BulkSheetLinkRequest request)
        {
            Db.InsertAll<SheetLinkDto>(request.SheetLinks);
            return new HttpResult(HttpStatusCode.Created);
        }

        public HttpResult Put(PublishSheetRequest request)
        {
            var ex = Db.ExecuteNonQuery("update mp_sheets set published_at = @publishedAt where id = @id", new { id = request.Id, publishedAt = DateTime.Now });
            return new HttpResult(HttpStatusCode.OK);
        }

        public HttpResult Delete(SheetLinkRequest request)
        {
            Db.Delete<SheetLinkDto>(sl => sl.SheetId == request.SheetId);
            return new HttpResult(HttpStatusCode.OK);
        }
    }
}

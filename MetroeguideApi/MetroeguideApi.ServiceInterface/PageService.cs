﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Chronos.ProtoBuffers;
using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using StackExchange.Redis;
using Dapper;

namespace MetroeguideApi.ServiceInterface
{
    public class PageService : Service
    {
        public new IDatabase Redis { get; set; }

        public long Post(PageDto page)
        {
            if(page.Id != 0)
            {
                var newId = Db.Insert<PageDto>(page);
                return newId;
            }
            else
            {
                var iden = Db.ExecuteScalar<int>(@"insert into pages (title, folder_id) 
                                                    values (@title, @folderId) returning id;",
                                                new { title = page.Title, folderId = page.FolderId });
                return iden;
            }
        }

        public HttpResult Put(PageDto page)
        {
            const string sql = @"update pages set 
title = coalesce(@title, title), 
auto_ordering = coalesce(@autoOrdering, auto_ordering), 
footer_html = coalesce(@footerHtml, footer_html), 
header_html = coalesce(@headerHtml, header_html),
meta_keys = coalesce(@metaKeys, meta_keys),
meta_desc = coalesce(@metaDesc, meta_desc),
route = coalesce(@route, route),
canonical_url = coalesce(@canonicalUrl, canonical_url),
comments = coalesce(@comments, comments),
zipcode = coalesce(@zipcode, zipcode),
modified = @modified
where id = @id";

            if (page.Id != default(int))
            {
                Db.ExecuteNonQuery(sql, new
                {
                    title = page.Title,
                    autoOrdering = page.AutoOrdering,
                    footerHtml = page.FooterHtml,
                    headerHtml = page.HeaderHtml,
                    metaKeys = page.MetaKeys,
                    metaDesc = page.MetaDesc,
                    route = page.Route,
                    canonicalUrl = page.CanonicalUrl,
                    comments = page.Comments,
                    zipcode = page.Zipcode,
                    modified = DateTime.UtcNow,
                    id = page.Id
                });
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Put(BulkPageUpdate request)
        {
            try
            {
                var sql = @"update pages set folder_id = @newFolderId WHERE id in (" + request.PageIds + ")";
                Db.ExecuteNonQuery(sql, new { newFolderId = request.NewParentFolderId });
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            } catch(Exception ex)
            {
                return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
            }
        }


        public PageResponse Get(PageRequest request)
        {
            var sw = Stopwatch.StartNew();

            //var cachedResponse = default(byte[]);
            //var requestKey = request.ToCacheKey("urn:Page:");

            //if (CacheHelpers.HasCacheEntry(Redis, requestKey, Request.QueryString, out cachedResponse))
            //{
            //    return CacheHelpers.UnwrapResponse<PageResponse>(cachedResponse, Response, sw);
            //}

            var result = default(List<PageDto>);

            request.OrderBy = request.OrderBy ?? new[] {"Title"};
            request.Limit = request.Limit ?? 100;

            var expression = Db.From<PageDto>();

            if (request.Id != default(int))
            {
                result = new[] {Db.Single<PageDto>(x => x.Id == request.Id)}
                    .ToList();
            }
            else if (request.FolderId != default(int))
            {
                expression = expression.Where(x => x.FolderId == request.FolderId);
            }
            else if (request.Route != default(string))
            {
                expression = expression
                    .Where(x =>
                        (x.Route == request.Route || request.Route == null)
                        &&
                        (x.CanonicalUrl == request.CanonicalUrl || request.CanonicalUrl == null)
                        &&
                        (x.Title == request.Title || request.Title == null)
                        &&
                        (x.Zipcode == request.Zipcode || request.Zipcode == null));
            }

            expression = request.OrderByDesc
                ? expression.OrderByFieldsDescending(request.OrderBy)
                : expression.OrderByFields(request.OrderBy);

            expression = expression.Limit(request.Skip, request.Limit);

            if (result == default(List<PageDto>))
            {
                result = Db.Select<PageDto>(expression);
            }

            var response = request.ConvertTo<PageResponse>();
            response.Result = result;
            response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            //var cacheObj = request.ToProtoBufCacheObj(response, "urn:Page:");
            //Redis.StringSet(requestKey, (RedisValue) cacheObj, TimeSpan.FromDays(14));
            return response;
        }

        public PageResponse Get(FindPageRequest request)
        {
            var result = default(List<PageDto>);

            if (request.FindById != 0)
            {
                result = new[] { Db.Single<PageDto>(x => x.Id == request.FindById) }.ToList();
            }
            else if (!string.IsNullOrEmpty(request.FindByName))
            {
                var expression = Db.From<PageDto>().Where(x => x.Title.Contains(request.FindByName)).OrderBy(x => x.Title);
                result = Db.Select<PageDto>(expression);
            }

            var response = new PageResponse();
            response.Result = result;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }

        public HttpResult Delete(DeletePageRequest request)
        {
            if (request.Id == default(int))
            {
                throw new HttpError(HttpStatusCode.BadRequest, "1", "Can only delete pages with an id");
            }


            Db.DeleteById<PageDto>(request.Id);
            return new HttpResult
            {
                StatusCode = HttpStatusCode.NoContent
            };
        }
    }
}
using System.Diagnostics;
using Chronos.ProtoBuffers;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack.Web;
using StackExchange.Redis;

namespace MetroeguideApi.ServiceInterface
{
    public static class CacheHelpers
    {
        public static bool HasCacheEntry(IDatabase redis,string cacheKey, INameValueCollection queryString, out byte[] cachedRes)
        {
            cachedRes =(byte[])redis.StringGet(cacheKey);
            return (cachedRes != null && queryString["bust"] != "true");
        }
        public static T UnwrapResponse<T>(byte[] cachedRes,IResponse response, Stopwatch sw ) where T : IHasTimings
        {
            var cacheWrapper = cachedRes.FromProtoBufByteArray<ProtoBufCacheWrapper>();
            response.AddHeader("X-Cached-From", cacheWrapper.CacheDate.ToString("O"));
            var res = cacheWrapper.ProtoBuffObject.FromProtoBufByteArray<T>();
            res.TimeTakenMs = sw.ElapsedMilliseconds;
            return res;
        }
    }
}
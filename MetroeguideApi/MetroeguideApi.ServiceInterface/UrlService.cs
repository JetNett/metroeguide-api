﻿using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceInterface
{
    public class UrlService : Service
    {

        //public HttpResult Put(UpdateUrlRequest request)
        //{
        //    var originalUrl = "";
        //}

        public UrlResponse Get(UrlRequest request)
        {
            var response = new UrlResponse();
            response.Result = new List<UrlDto>();

            if (request.Id != default(int))
            {
                var url = Db.Single<UrlDto>(x => x.Id == request.Id);
                if(url != null)
                {
                    response.Result.Add(url);
                }
            }

            return response;
        } 

        public HttpResult Put(SuspendUrlRequest request)
        {

            try
            {
                var sql = "update urls set suspended = @Suspended where id = @UrlId";
                Db.ExecuteNonQuery(sql, new { Suspended = request.Suspended, UrlId = request.Id });

                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch(Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Put(UpdateUrlRequest urlObj)
        {
            const string sql = @"update urls set url = @Url where id = @Id";

            try
            {
                var url = Get(new UrlRequest { Id = urlObj.Id }).Result.FirstOrDefault();
                
                if(url == null)
                {
                    return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
                }

                var urlUpdated = url.Url != urlObj.Url;

                if (urlObj.Id != default(int))
                {
                    Db.ExecuteNonQuery(sql, new
                    {
                        Url = urlObj.Url,
                        Id = urlObj.Id
                    });

                   return new HttpResult("Ok", HttpStatusCode.NoContent);
                }

            }
            catch (Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Put(CommentUrlRequest request)
        {
            const string sql = "update urls set comments = @Comment where id = @Id";
            try
            {
                Db.ExecuteNonQuery(sql, new
                {
                    Comment = request.Comment,
                    Id = request.Id
                });

               return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch (Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Delete(RemoveUrlRequest request)
        {

            try
            {
                if(request.Id == default(int))
                    return new HttpResult("Bad Request", HttpStatusCode.BadRequest);

                Db.DeleteById<UrlDto>(request.Id);

                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch(Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

    }

}

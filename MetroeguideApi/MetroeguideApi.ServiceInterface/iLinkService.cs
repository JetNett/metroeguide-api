﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Chronos.ProtoBuffers;
using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using StackExchange.Redis;
using Dapper;
using Npgsql;
using System.Net;

namespace MetroeguideApi.ServiceInterface
{
    public class iLinkService : Service
    {
        public new IDatabase Redis { get; set; }

        public iLinkCommunityProfileResponse Get(iLinkCommunityProfileRequest request)
        {
            var sw = Stopwatch.StartNew();

            var cps = Db.Select<int>("select page_id from community_profiles_mappings where client_id = @clientId",
                new {clientId = request.ClientId}).ToList();

            request.OrderBy = request.OrderBy ?? new[] {"Title"};

            var expression = Db.From<PageDto>()
                .Where(x => cps.Contains(x.Id));

            expression = request.OrderByDesc
                ? expression.OrderByFieldsDescending(request.OrderBy)
                : expression.OrderByFields(request.OrderBy);

            var result = Db.Select<PageDto>(expression);
            
            var response = request.ConvertTo<iLinkCommunityProfileResponse>();
            response.Result = result;
            response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }

        public iLinkResponse Get(iLinkRequest request)
        {
            var sw = Stopwatch.StartNew();

            //var cachedResponse = default(byte[]);
            //var requestKey = request.ToCacheKey();

            //if (CacheHelpers.HasCacheEntry(Redis, requestKey, Request.QueryString, out cachedResponse))
            //{
            //    return CacheHelpers.UnwrapResponse<iLinkResponse>(cachedResponse, Response, sw);
            //}

            var result = default(List<iLinkDto>);
            request.OrderBy = request.OrderBy ?? new[] { "ClientId" };
            request.Limit = request.Limit ?? 100;

            var expression = Db.From<iLinkDto>();

            if (request.ClientId != default(int))
            {
                result = new[] { Db.Single<iLinkDto>(x => x.ClientId == request.ClientId) }
                    .ToList();
            }

            expression = request.OrderByDesc
                ? expression.OrderByFieldsDescending(request.OrderBy)
                : expression.OrderByFields(request.OrderBy);

            expression = expression.Limit(request.Skip, request.Limit);

            if (result == default(List<iLinkDto>))
            {
                result = Db.Select<iLinkDto>(expression);
            }

            var response = request.ConvertTo<iLinkResponse>();
            response.Result = result;
            response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            //cache the respone
            //var cacheObj = request.ToProtoBufCacheObj(response);
            //Redis.StringSet(requestKey, (RedisValue)cacheObj, TimeSpan.FromDays(14));

            return response;
        }

        public HttpResult Post(iLinkDto iLink)
        {

            const string sql = @"insert into i_links (
                                    client_id,
                                    client_logo_image_src,
                                    client_logo_image_href,
                                    client_logo_image_target,
                                    product_logo_image_src,
                                    product_logo_image_href,
                                    product_logo_image_target,
                                    back_anchor_text,
                                    back_anchor_url,
                                    back_anchor_target,
                                    modified
                                )
                                VALUES (
                                    @ClientId,
                                    @ClientLogoImageSrc,
                                    @ClientLogoImageHref,
                                    @ClientLogoImageTarget,
                                    @ProductLogoImageSrc,
                                    @ProductLogoImageHref,
                                    @ProductLogoImageTarget,
                                    @BackAnchorText,
                                    @BackAnchorUrl,
                                    @BackAnchorTarget,
                                    now()
                                )";

            try
            {
                Db.Execute(sql, iLink);
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch (Exception ex) { 
                return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
            }
        }
            
        public HttpResult Put(iLinkDto iLink)
        {
            const string sql = @"update i_links set
                                client_logo_image_src = COALESCE(@clientLogoImageSrc, client_logo_image_src),
                                client_logo_image_href = COALESCE(@clientLogoImageHref, client_logo_image_href),
                                client_logo_image_target = COALESCE(@clientLogoImageTarget, client_logo_image_target),
                                product_logo_image_src = COALESCE(@productLogoImageSrc, product_logo_image_src),
                                product_logo_image_href = COALESCE(@productLogoImageHref, product_logo_image_href),
                                product_logo_image_target = COALESCE(@productLogoImageTarget, product_logo_image_target),
                                back_anchor_text = COALESCE(@backAnchorText, back_anchor_text),
                                back_anchor_url = COALESCE(@backAnchorUrl, back_anchor_url),
                                back_anchor_target = COALESCE(@backAnchorTarget, back_anchor_target),
                                modified = now()
                                where id = @id";
            
            if(iLink.Id != default(int))
            {
                Db.ExecuteNonQuery(sql, new
                {
                    id = iLink.Id,
                    clientLogoImageSrc = iLink.ClientLogoImageSrc,
                    clientLogoImageHref = iLink.ClientLogoImageHref,
                    clientLogoImageTarget = iLink.ClientLogoImageTarget,
                    productLogoImageSrc = iLink.ProductLogoImageSrc,
                    productLogoImageHref = iLink.ProductLogoImageHref,
                    productLogoImageTarget = iLink.ProductLogoImageTarget,
                    backAnchorText = iLink.BackAnchorText,
                    backAnchorUrl = iLink.BackAnchorUrl,
                    backAnchorTarget = iLink.BackAnchorTarget
                });

                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Delete(DeleteILinkRequest iLink)
        {
            if (iLink.Id == default(int))
            {
                throw new HttpError(HttpStatusCode.BadRequest, "1", "Can only delete ilinks with an id");
            }

            try
            {
                Db.DeleteById<iLinkDto>(iLink.Id);

                return new HttpResult
                {
                    StatusCode = HttpStatusCode.NoContent
                };
            } catch (Exception ex)
            {
                return new HttpResult("Bad Result", HttpStatusCode.BadRequest);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using ServiceStack.Web;
using Npgsql;
using Dapper;
using System.Net;

namespace MetroeguideApi.ServiceInterface
{
    public class FolderService : Service
    {
        public FolderResponse Get(FindFolderRequest request)
        {
            var result = default(List<FolderDto>);

            if(request.FindById != 0)
            {
                result = new[] { Db.Single<FolderDto>(x => x.Id == request.FindById) }.ToList();
            }
            else if (!string.IsNullOrEmpty(request.FindByName))
            {
                var expression = Db.From<FolderDto>().Where(x => x.Name.Contains(request.FindByName)).OrderBy(x => x.Name);
                result = Db.Select<FolderDto>(expression);
            }

            var response = new FolderResponse();
            response.Result = result;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }

        public FolderResponse Get(FolderRequest request)
        {
            var sw = Stopwatch.StartNew();
            
            var result = default(List<FolderDto>);
            request.OrderBy = request.OrderBy ?? new[] { "Name" };
            request.Limit = request.Limit ?? 100;

            var expression = Db.From<FolderDto>();

            if (request.Id != default(int))
            {
                result = new[] { Db.Single<FolderDto>(x => x.Id == request.Id) }
                    .ToList();
            }
            if (request.Name != default(string))
            {
                expression = expression.Where(x => x.Name == request.Name);
            }
            if (request.ParentFolderId != default(int?))
            {
                if (request.ParentFolderId == -1)
                {
                    expression = expression.Where(x => x.ParentFolderId == null);
                }
                else
                {
                    expression = expression.Where(x => x.ParentFolderId == request.ParentFolderId);
                }
            }

            expression = request.OrderByDesc
                ? expression.OrderByFieldsDescending(request.OrderBy)
                : expression.OrderByFields(request.OrderBy);

            expression = expression.Limit(request.Skip, request.Limit);

            if (result == default(List<FolderDto>))
            {
                result = Db.Select<FolderDto>(expression);
            }

            var response = request.ConvertTo<FolderResponse>();
            response.Result = result;
            response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }

        public long Post(FolderDto folder)
        {
            var iden = Db.ExecuteScalar<int>("insert into folders (name, parent_folder_id) values (@Name, @ParentFolderId) returning id;", folder);
            return iden;
        }

        [Route("/folderhierarchy")]
         public FolderHierarchyResponse Get(FolderHierarchyRequest request)
        {
            string getRecursivePostgres = @"with RECURSIVE tblParent as 
                                                        (
                                                            SELECT * FROM folders where Id = @id
                                                            UNION ALL
                                                            SELECT f.* from folders as f JOIN tblParent on f.Id = tblParent.parent_folder_id
                                                        )
                                                        select id, name, parent_folder_id as parentFolderId from tblParent order by parent_folder_id NULLS FIRST";

            using (var db = new NpgsqlConnection(Db.ConnectionString))
            {
                var folders = db.Query<FolderDto>(getRecursivePostgres, new { id = request.Id }).ToList();
                var response = request.ConvertTo<FolderHierarchyResponse>();
                response.Result = folders;
                return response;
            }
        }

        public HttpResult Delete(DeleteFolderRequest request)
        {
            if (request.Id == default(int))
            {
                throw new HttpError(HttpStatusCode.BadRequest, "1", "Can only delete folders with an id");
            }

            Db.DeleteById<FolderDto>(request.Id);
            return new HttpResult
            {
                StatusCode = HttpStatusCode.NoContent
            };
        }
    }
}
﻿using Dapper;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceInterface
{
    public class CommunityProfileService : Service
    {
        public HttpResult Post(CommunityProfileDto communityProfile)
        {
            try
            {
                var sql = "INSERT INTO community_profiles_mappings (client_id, page_id) VALUES (@ClientId, @PageId)";
                Db.Execute(sql, communityProfile);
                return new HttpResult("Created", HttpStatusCode.Created);
            }
            catch (Exception ex)
            {
                return new HttpResult("BadRequest", HttpStatusCode.BadRequest);
            }
        }

        public HttpResult Delete(CommunityProfileDto communityProfile)
        {
            try
            {
                var sql = "DELETE FROM community_profiles_mappings WHERE client_id = @ClientId and page_id = @PageId";
                Db.Execute(sql, communityProfile);
                return new HttpResult("NoContent", HttpStatusCode.NoContent);
            } catch (Exception ex)
            {
                return new HttpResult("BadRequest", HttpStatusCode.BadRequest);
            }

        }
    }

    public class CommunityProfileDto
    {
        public int ClientId { get; set; }
        public int PageId { get; set; }
    }
}

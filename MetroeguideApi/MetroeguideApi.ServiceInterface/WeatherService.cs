﻿using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceInterface
{
    public class WeatherService : Service
    {
        private static string ApiKey = "72c9a7f4c463096f7240088fb457b457";
        public WeatherResponse Get(WeatherRequest request)
        {
            var url = "http://api.openweathermap.org/data/2.5/weather?appid=" + ApiKey + "&units=imperial&zip=" + request.Zipcode + ",us";
            using (var client = new WebClient())
            {
                var content = client.DownloadString(url);
                var weatherObj = JsonConvert.DeserializeObject<WeatherDto>(content);
                if(weatherObj.Weather.Length > 0)
                {
                    try
                    {
                        var response = new WeatherResponse
                        {
                            CurrentCondition = weatherObj.Weather[0].Main,
                            CurrentTemp = Convert.ToInt32(weatherObj.TempDetails.Temp),
                            MaxTemp = Convert.ToInt32(weatherObj.TempDetails.TempMax),
                            MinTemp = Convert.ToInt32(weatherObj.TempDetails.TempMin),
                            IconId = weatherObj.Weather[0].Id
                        };
                        return response;
                    } catch(Exception ex) { }
                }
                return null;
            }
        }
    }
}

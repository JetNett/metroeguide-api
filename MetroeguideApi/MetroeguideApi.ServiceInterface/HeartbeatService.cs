﻿using System;
using ServiceStack;
using StackExchange.Redis;

namespace MetroeguideApi.ServiceInterface
{
    public class HeartbeatService : Service
    {
        public new IDatabase Redis { get; set; }
        public object Get(Heartbeat request)
        {
            Redis.HashSet("urn:Heartbeats", request.Key ?? "none", DateTime.UtcNow.ToString("O"));
            return "PONG!";
        }
    }

    public class Heartbeat
    {
        public string Key { get; set; }
    }
}
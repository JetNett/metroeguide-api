﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Chronos.ProtoBuffers;
using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using StackExchange.Redis;
using Npgsql;
using Dapper;
using System.Net;

namespace MetroeguideApi.ServiceInterface
{
    public class ClientService : Service
    {
        public new IDatabase Redis { get; set; }
        public ClientResponse Get(ClientRequest request)
        {
            var sw = Stopwatch.StartNew();

            //var cachedResponse = default(byte[]);
            //var requestKey = request.ToCacheKey();

            //if (CacheHelpers.HasCacheEntry(Redis, requestKey, Request.QueryString, out cachedResponse))
            //{
            //    return CacheHelpers.UnwrapResponse<ClientResponse>(cachedResponse, Response, sw);
            //}

            var result = default(List<ClientDto>);
            request.OrderBy = request.OrderBy ?? new[] { "Name" };
            request.Limit = request.Limit ?? 100;

            var expression = Db.From<ClientDto>();

            if (request.Id != default(int))
            {
                result = new[] { Db.Single<ClientDto>(x => x.Id == request.Id) }
                    .ToList();
            }
            else if (request.Condensed)
            {
                var condensedQuery = "SELECT id, name, modified FROM clients ORDER BY name ASC";
                using (var db = new NpgsqlConnection(Db.ConnectionString))
                {
                    result = db.Query<ClientDto>(condensedQuery).ToList();
                }
            }

            expression = request.OrderByDesc
                ? expression.OrderByFieldsDescending(request.OrderBy)
                : expression.OrderByFields(request.OrderBy);

            expression = expression.Limit(request.Skip, request.Limit);

            if (result == default(List<ClientDto>))
            {
                result = Db.Select<ClientDto>(expression);
            }

            var response = request.ConvertTo<ClientResponse>();
            response.Result = result;
            response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            //cache the respone
           // var cacheObj = request.ToProtoBufCacheObj(response);
          //  Redis.StringSet(requestKey, (RedisValue)cacheObj, TimeSpan.FromDays(14));
            return response;
        }

        public ClientResponse Post(ClientDto dto)
        {
            var sql = @"INSERT INTO clients (name, user_id, password, modified) VALUES (@Name, @UserId, @Password, now()) returning id;";
            var iden = Db.ExecuteScalar<int>(sql, dto);

            return Get(new ClientRequest { Id = iden });
        }


        public HttpResult Put(ClientDto client)
        {
            const string sql = @"UPDATE clients SET
                                 name = COALESCE(@name, name),
                                 user_id = COALESCE(@userId, user_id),
                                 password = COALESCE(@password, password),
                                 modified = now()
                                 WHERE id = @id";

            if (client.Id != default(int)) {
                Db.ExecuteNonQuery(sql, new
                {
                    id = client.Id,
                    name = client.Name,
                    userId = client.UserId,
                    password = client.Password
                });

                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Delete(DeleteClientRequest client)
        {
            if (client.Id == default(int))
            {
                throw new HttpError(HttpStatusCode.BadRequest, "1", "Can only delete clients with an id");
            }

            Db.DeleteById<ClientDto>(client.Id);

            return new HttpResult
            {
                StatusCode = HttpStatusCode.NoContent
            };
        }
    }
}
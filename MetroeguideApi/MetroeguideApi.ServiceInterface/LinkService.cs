﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Chronos;
using Chronos.Dapper.Chronos.Dapper;
using Chronos.ProtoBuffers;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using MetroeguideApi.ServiceModel;
using ServiceStack.OrmLite;
using ServiceStack.ProtoBuf;
using ServiceStack.Web;
using StackExchange.Redis;

namespace MetroeguideApi.ServiceInterface
{
    public class LinkService : Service
    {
        public new IDatabase Redis { get; set; }


        public HttpResult Put(LinkDto link)
        {
            const string sql = @"
update links set
    title = coalesce(@title, title),
    url = coalesce(@url, url),
    position = coalesce(@position, position),
    target = coalesce(@target, target),
    is_link = coalesce(@isLink, is_link),
    comments = coalesce(@comments, comments),
    modified = @modified
where id = @id";

            if (link.Id != default(int))
            {
                Db.ExecuteNonQuery(sql, new
                {
                    title = link.Title,
                    url = link.Url,
                    target = link.Target,
                    isLink = link.IsLink,
                    comments = link.Comments,
                    id = link.Id,
                    position = link.Position,
                    modified = DateTime.UtcNow
                });
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Put(BulkLinkRequest links)
        {
            try
            {
                const string sql = @"update links set 
                                        position = coalesce(@position, position),
                                        url = coalesce(@url, url),
                                        title = coalesce(@title, title),
                                        comments = coalesce(@comments, comments),
                                        is_suspended = coalesce(@isSuspended, is_suspended),
                                        is_elevated = coalesce(@isElevated, is_elevated)
                                    WHERE id = @id";


                foreach(var link in links.Links)
                {
                    Db.ExecuteNonQuery(sql, new {
                        id = link.Id,
                        position = link.Position,
                        url = link.Url,
                        title = link.Title,
                        comments = link.Comments,
                        isSuspended = link.IsSuspended,
                        isElevated = link.IsElevated
                    });
                }
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch(Exception ex)
            {
                return new HttpResult("InternalServerError", HttpStatusCode.InternalServerError);
            }
        }

        public long Post(LinkDto request)
        {
            var iden = Db.ExecuteScalar<int>("insert into links (title, url,url_id, is_link, page_id, position, target, comments, modified) values (@title, @url,0, @islink, @pageid, @position, @target, @comments, current_timestamp) returning id;", request);
            return iden;
        }


        public HttpResult Post(BulkLinkRequest request)
        {
            Db.InsertAll<LinkDto>(request.Links);
            return new HttpResult("Ok", HttpStatusCode.Created);
        }

        public HttpResult Delete(DeleteLinkRequest request)
        {
            if (request.Id == default(int))
            {
                throw new HttpError(HttpStatusCode.BadRequest, "1", "Can only delete links with an id");
            }

            Db.DeleteById<LinkDto>(request.Id);
            return new HttpResult
            {
                StatusCode = HttpStatusCode.NoContent
            };
        }

        public HttpResult Delete(BulkLinkRequest request)
        {
            try
            {
                const string sql = @"delete from links where id = @id";
                foreach (var link in request.Links)
                {
                    Db.ExecuteNonQuery(sql, new { id = link.Id });
                }
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return new HttpResult("InternalServerError", HttpStatusCode.InternalServerError);
            }
        }

        public HttpResult Delete(BulkLinkDeleteRequest request)
        {
            try
            {
                const string sql = @"delete from links where page_id = @pageId";
                Db.ExecuteNonQuery(sql, new { pageId = request.PageId });
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch(Exception ex)
            {
                return new HttpResult("InternalServerError", HttpStatusCode.InternalServerError);
            }
        }

        public LinkResponse Get(LinkRequest request)
        {
            var sw = Stopwatch.StartNew();

            //var cachedResponse = default(byte[]);
            //var requestKey = request.ToCacheKey();

            //if (CacheHelpers.HasCacheEntry(Redis,requestKey, Request.QueryString,out cachedResponse))
            //{
            //    return CacheHelpers.UnwrapResponse<LinkResponse>(cachedResponse,Response, sw);
            //}
            var result = default(List<LinkDto>); 
            request.OrderBy = request.OrderBy ?? new[] {"PageId", "Position", "Title", "Url"};
            request.Limit = request.Limit ?? 100;

            var expression = Db.From<LinkDto>();

            if (request.Id != default(int))
            {
                result = new[] {Db.Single<LinkDto>(x => x.Id == request.Id)}
                    .ToList();
            }
            else if (request.PageId != default(int))
            {
                expression = expression.Where(x => x.PageId == request.PageId);
            }
            else if (request.UrlId != default(int))
            {
                expression = expression.Where(x => x.UrlId == request.UrlId);
            }
            else if (request.Title != null && request.Url != null)
            {
                expression = expression
                    .Where(x => x.Title == request.Title && x.Url == request.Url);
            }
            else if ((request.Title ?? request.Url) != null || (request.IsElevated ?? request.IsSuspended) != null)
            {
                expression = expression
                    .Where(x =>
                        (x.Title == request.Title || request.Title == null)
                        &&
                        (x.Url == request.Url || request.Url == null)
                        &&
                        (x.IsElevated == request.IsElevated || request.IsElevated == null)
                        &&
                        (x.IsSuspended == request.IsSuspended || request.IsSuspended == null));
                 
            }
            expression = request.OrderByDesc 
                ? expression.OrderByFieldsDescending(request.OrderBy) 
                : expression.OrderByFields(request.OrderBy);

            expression = expression.Limit(request.Skip, request.Limit);

            if (result == default(List<LinkDto>))
            {
                result = Db.Select<LinkDto>(expression);
            }

            var response = request.ConvertTo<LinkResponse>();
            response.Result = result;
            response.TimeTakenMs = sw.ElapsedMilliseconds;
            response.ResultCount = result != null ? result.Count : 0;

            //cache the respone
            //var cacheObj = request.ToProtoBufCacheObj(response);
            //Redis.StringSet(requestKey, (RedisValue)cacheObj, TimeSpan.FromDays(14));

            return response;
        }

        public LinkResponse Get(FindLinksRequest request)
        {
            var result = default(List<LinkDto>);
            var expression = Db.From<LinkDto>();

            if(!string.IsNullOrEmpty(request.Title) && !string.IsNullOrEmpty(request.Url))
            {
                expression.Where(x => x.Title == request.Title && x.Url == request.Url);
            }
            else if (!string.IsNullOrEmpty(request.Title) && string.IsNullOrEmpty(request.Url))
            {
                expression.Where(x => x.Title == request.Title);
            }
            else if (!string.IsNullOrEmpty(request.Title) && string.IsNullOrEmpty(request.Url) && request.UseSimilar)
            {
                expression.Where(x => x.Title.Contains(request.Title));
            }
            else if(string.IsNullOrEmpty(request.Title) && !string.IsNullOrEmpty(request.Url))
            {
                expression.Where(x => x.Url == request.Url);
            }
            else if (string.IsNullOrEmpty(request.Title) && !string.IsNullOrEmpty(request.Url) && request.UseSimilar)
            {
                expression.Where(x => x.Url.Contains(request.Url));
            }

            result = Db.Select(expression);

            var response = new LinkResponse();
            response.Result = result;
            response.ResultCount = result != null ? result.Count : 0;

            return response;
        }
    }
}
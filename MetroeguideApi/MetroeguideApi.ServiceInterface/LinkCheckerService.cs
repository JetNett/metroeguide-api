﻿using MetroeguideApi.ServiceModel;
using MetroeguideApi.ServiceModel.Types;
using ServiceStack;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MetroeguideApi.ServiceInterface
{
    public class LinkCheckerService : Service
    {
        public CheckedLinkResponse Get(CheckedLinkRequest request)
        {
            var result = default(List<CheckedLinkDto>);

            request.Limit = request.Limit ?? 100;
            var query = Db.From<CheckedLinkDto>();

            query = query.OrderBy(x => x.TimeChecked);

            if (!string.IsNullOrEmpty(request.Id))
            {
                result = new[] { Db.Single<CheckedLinkDto>(x => x.Id == request.Id) }.ToList();
            }

            query = query.Limit(request.Skip, request.Limit);

            if (!string.IsNullOrEmpty(request.Type))
            {
                if (request.Type == "good")
                {
                    query = query.Where(x => x.Status == 200 && x.Url == x.EndUrl);
                }
                else if (request.Type == "unmatched")
                {
                    query = query.Where(x => x.Url != x.EndUrl);
                }
                else if(request.Type == "bad")
                {
                    query = query.Where(x => x.Status == 500);
                }
            }

            if (result == default(List<CheckedLinkDto>))
            {
                result = Db.Select<CheckedLinkDto>(query);
            }

            var _result = new List<CheckedLinkDto>();
            using (var urlService = base.ResolveService<UrlService>())
            {
                foreach (var r in result)
                {
                    var url = urlService.Get(new UrlRequest { Id = r.UrlId });
                    if(url.Result.Count > 0)
                    {
                        r.Comments = url.Result[0].Comments;
                        _result.Add(r);
                    }
                }
            }

            var response = new CheckedLinkResponse();
            response.Result = _result;
            response.ResultCount = _result != null ? _result.Count : 0;
            response.Skip = request.Skip;
            response.Limit = request.Limit;

            return response;
        }

        public HttpResult Put(CheckedLinkUrlRequest request)
        {
            //Validate we have the data we need.
            if (string.IsNullOrEmpty(request.Id) || string.IsNullOrEmpty(request.Url) || request.UrlId == default(int))
                return new HttpResult("Bad Request", HttpStatusCode.BadRequest);

            //get the url service to work with it.
            using (var urlService = base.ResolveService<UrlService>())
            {
                //attempt to get the original url object based on the id
                var originalUrl = "";
                var url = urlService.Get(new UrlRequest { Id = request.UrlId }).Result.FirstOrDefault();
                if (url != null)
                    originalUrl = url.Url;

                //resolve the link service so that we can call its bulk update
                using(var linkService = base.ResolveService<LinkService>())
                {
                    //find all links
                    var linkResponse = linkService.Get(new FindLinksRequest { Url = originalUrl });

                    if (linkResponse.ResultCount > 0)
                    {
                        var links = new List<LinkDto>();

                        //if we have links - set the url to the new url
                        foreach (var link in linkResponse.Result)
                        {
                            link.Url = request.Url;
                            links.Add(link);
                        }

                        //actualy run the bulk update
                        var linkUpdateResponse = linkService.Put(new BulkLinkRequest
                        {
                            Links = links
                        });

                        if (linkUpdateResponse.Status == 204)
                        {
                            //update log item 
                            var linkLogItem = Db.Select<CheckedLinkDto>(x => x.Id == request.Id);
                            var sql = "update link_checker_logs_new set url = @Url where id = @Id";

                            Db.ExecuteNonQuery(sql, new { Url = request.Url, Id = request.Id });


                            //update url value
                            var urlServiceResponse = urlService.Put(new UpdateUrlRequest
                            {
                                Id = request.UrlId,
                                Url = request.Url
                            });

                            if(urlServiceResponse.Status == 204)
                            {
                                return new HttpResult("Ok", HttpStatusCode.NoContent);
                            }
                        }
                    }
                }
            }


            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Put(SuspendCheckedLinkRequest request)
        {
            try
            {
                using (var linkService = base.ResolveService<LinkService>())
                {
                    var linkResponse = linkService.Get(new FindLinksRequest
                    {
                        Url = request.Url
                    });

                    var links = new List<LinkDto>();
                    if(linkResponse.Result.Count > 0)
                    {
                        foreach(var link in linkResponse.Result)
                        {
                            link.IsSuspended = true;
                            link.Modified = DateTime.Now;
                            links.Add(link);
                        }
                    }

                    var updateLinkResponse = linkService.Put(new BulkLinkRequest
                    {
                        Links = links
                    });

                    if(updateLinkResponse.Status == 204)
                    {
                        using (var urlService = base.ResolveService<UrlService>())
                        {
                            var updateUrlResponse = urlService.Put(new SuspendUrlRequest
                            {
                                Id = request.UrlId,
                                Suspended = true
                            });

                            if(updateUrlResponse.Status == 204)
                            {
                                return new HttpResult("Ok", HttpStatusCode.NoContent);
                            }
                        }
                    }
                }
            }
            catch(Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Post(CreateWhitelistedUrlRequest request)
        {
            try
            {
                var whitelistDto = new WhitelistedUrlDto();
                whitelistDto.DateAdded = DateTime.Now;
                whitelistDto.DateExpires = DateTime.Now.AddDays(30);

                whitelistDto.Id = Guid.NewGuid();
                whitelistDto.UrlId = request.UrlId;

                Db.Insert(whitelistDto);

                return new HttpResult("Ok", HttpStatusCode.Created);
            }
            catch(Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Delete(DeleteCheckedLinkRequest request)
        {

            if (string.IsNullOrEmpty(request.Id))
                return new HttpResult("Bad Request", HttpStatusCode.BadRequest);

            try
            {
                Db.DeleteById<CheckedLinkDto>(request.Id);
                return new HttpResult("Ok", HttpStatusCode.NoContent);
            }
            catch(Exception ex) { }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Delete(DeactivateLinkRequest request)
        {
            using (var linkService = base.ResolveService<LinkService>())
            {

                var linkResponse = linkService.Get(new FindLinksRequest
                {
                    Url = request.Url
                });

                if (linkResponse.Result.Count > 0)
                {
                    var deleteLinksResponse = linkService.Delete(new BulkLinkRequest
                    {
                        Links = linkResponse.Result
                    });

                    if (deleteLinksResponse.Status == 204)
                    {
                        using (var urlService = base.ResolveService<UrlService>())
                        {
                            var urlDeleteResponse = urlService.Delete(new RemoveUrlRequest { Id = request.UrlId });
                            if (urlDeleteResponse.Status == 204)
                                return new HttpResult("Ok", HttpStatusCode.NoContent);
                        }
                    }
                }
            };

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }

        public HttpResult Put(CommentCheckedLinkRequest request)
        {
            using (var linkService = base.ResolveService<LinkService>())
            {
                var linkResponse = linkService.Get(new FindLinksRequest
                {
                    Url = request.Url
                });

                if (linkResponse.Result.Count > 0)
                {
                    var links = new List<LinkDto>();
                    foreach (var link in linkResponse.Result)
                    {
                        link.Comments = request.Comment;
                        links.Add(link);
                    }

                    var updateLinkResponse = linkService.Put(new BulkLinkRequest
                    {
                        Links = links
                    });

                    if(updateLinkResponse.Status == 204)
                    {
                        using (var urlService = base.ResolveService<UrlService>())
                        {
                            var updateUrlCommentResponse = urlService.Put(new CommentUrlRequest
                            {
                                Id = request.UrlId,
                                Comment = request.Comment
                            });

                            if(updateUrlCommentResponse.Status == 204)
                                return new HttpResult("Ok", HttpStatusCode.NoContent);
                        }
                    }
                }
            }

            return new HttpResult("Bad Request", HttpStatusCode.BadRequest);
        }
    }

}

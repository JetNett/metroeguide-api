﻿using System;
using NUnit.Framework;
using MetroeguideApi.ServiceInterface;
using MetroeguideApi.ServiceModel;
using ServiceStack.Testing;
using ServiceStack;

namespace MetroeguideApi.Tests
{
    [TestFixture]
    public class UnitTests
    {
        private readonly ServiceStackHost appHost;

        public UnitTests()
        {
            appHost = new BasicAppHost(typeof(LinkService).Assembly)
            {
                ConfigureContainer = container =>
                {
                    //Add your IoC dependencies here
                }
            }
            .Init();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            appHost.Dispose();
        }

        [Test]
        public void TestMethod1()
        {
            var service = appHost.Container.Resolve<LinkService>();

            //var response = (LinkResponse)service.Any(new LinkRequest { Name = "World" });

            //Assert.That(response.Result, Is.EqualTo("Hello, World!"));
        }
    }
}
